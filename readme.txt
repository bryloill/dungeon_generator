
#### Adresařová struktura:

|readme.txt 
|-src
| |scripts           zdrojové kob dy implementace
| |examples          .blend soubory ukázek
|-text               text práce
| |thesis.pdf        text práce ve formátu PDF

#### Spuštění
    Ve složce src se nachází složky scripts, examples a tests. 
    V examples je na výběr ke spuštění několik Blender souborů.

Obsah .blend souborů:
    2d.blend: generování plánku dungeonu pomocí sady 2D obrázků,
    3d.blend: generování mapy dungeonu pomocí navržené sady 3D objektů, 
    land.blend: generování mapy terénu pomocí sady 5 různých políček, 
    test.blend: testováni implementace.

Po otevření .blend souboru stačí přejít do layoutu Scripting. V něm se už nachází uložená varianta skriptu generator.py. 
Jediné, co zbývá nastavit, je proměnná packages_path, která je v horní častí skriptu. 
Proměnná musí být nastavena na absolutní cestu do přemístěné složky src/scripts. 
V případě systému Windows je potřeba escapovat zpětná lomítka. Tato proměnná se použije proto, 
aby prostředí Blenderu vědělo, kde hledat další soubory, ze kterých je skript sestaven.
Následně zbývá jenom zmáčknout tlačítko Run Script a pozorovat proces generování.
Pro spuštění generovaní od začátku je potřeba vymazat ze scény předchozí výsledek.
Mezi každém spuštěním je možné měnit parametry ve slovníku map_info. 
Jsou tak třeba nastavitelné pravděpodobnosti jednotlivých druhů objektů. 
Součet nastavených pravděpodobností musí být 1.

#### Úprava skriptu
Implementace umožňuje napsání vlastních generátorů pro jiné sady objektů. 
Při použití jiné sady objektů je potřeba nejdříve takovou sadu vytvořit nebo najít. 
Objekty v sadě musí mít čtvercový tvar a být schopné na sebe navazovat.
Ve složce skripts se nachází složka templates, 
kde jsou k nalezení dvě šablony - jedna pro vstupní bod skriptu a druhá pro vlastní implementaci generátoru.
Po jakékoliv úpravě souborů s implementací (ne úpravách proměnných) je potřeba restartovat Blender, 
jinak se změny v kódu neprojeví.

    ++++ Úprava sady
    Sada se musí umístit do scény v Blenderu do kolekce Source. Budou to klasické objekty Blenderu
    se stejnou velikostí. Počátek každého objektu se musí nacházet v jeho centru.

    ++++ Vlastní implementace souborů generate_*.py
    Jako příklad může být vzat jakýkoliv existující soubor s názvem generate_*.py a může být použita nabídnutá šablona. 
    V této šabloně je potřeba definovat 4 funkce: create_prototypes, set_valid_neighbours, entropy_func, choice_func. 
    Kromě funkcí je třeba vyplnit slovník raw a případně dodefinovat třídu Type.

    ++++ Úprava souboru generator.py
    V šabloně pro generator.py se musí nastavit řádek importu, aby byla naimportována správně
    implementace. Kromě toho se musí aktualizovat slovník map_info podle potřeb. 
    
#### Spuštění testů
Testy se dají spustit a nastavit v souboru test.blend. Skript je spustitelný obdobně, jako bylo popsáno na začátku návodu. 
Parametry testování se dají nastavit přes proměnné slovníku map_info. 
ROUNDS nastavuje počet spuštění generování, než se zobrazí výsledky testování.
Pro zobrazení výsledků testování musí být otevřena příkazová řádka (Toggle System Console). 
Pokud taková možnost není, je také možné spustit Blender přes systémovou příkazovou řádku. V takovém případě se výpisy testů budou objevovat v ní.
