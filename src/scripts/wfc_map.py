import random
import bpy
from utils import Pos, ConflictException, get_side
from tile_prototype import TilePrototype


class WFCMap:
    tiles: list
    x_length: int
    y_length: int
    start_x: int
    start_y: int
    tile_size_x: int
    tile_size_y: int
    __max_entropy: int
    __collapsed: set

    def __init__(self, map_info, prototypes):

        if map_info['X_LENGTH'] <= 2 or map_info['X_LENGTH'] <= 2 \
                or map_info['TILE_SIZE_X'] <= 0 or map_info['TILE_SIZE_Y'] <= 0:
            raise ValueError

        self.x_length = map_info['X_LENGTH']
        self.y_length = map_info['Y_LENGTH']
        self.start_x = map_info['START_X']
        self.start_y = map_info['START_Y']
        self.tile_size_x = map_info['TILE_SIZE_X']
        self.tile_size_y = map_info['TILE_SIZE_Y']
        self.__result_collection_name = map_info['RESULT_COLLECTION_NAME']
        self.__max_entropy = len(prototypes)
        self.__collapsed = set()
        self.tiles = [[prototypes.copy() for y in range(self.y_length)] for x in range(self.x_length)]

    @staticmethod
    def get_entropy(tile) -> int:
        pass

    @staticmethod
    def weighted_choice(prototypes) -> TilePrototype:
        pass

    def iterate(self):
        position = self.__get_min_entropy_position()
        self.__collapse(position)
        self.__propagate(position)

    def is_collapsed(self):
        return len(self.__collapsed) == self.x_length * self.y_length

    def __collapse(self, position):
        prototype = WFCMap.weighted_choice(list(self.tiles[position.x][position.y]))

        if position in self.__collapsed:
            raise Exception('Already used position')

        self.__collapsed.add(position)
        self.tiles[position.x][position.y] = {prototype}
        self.__show_tile(prototype, position)

    def __propagate(self, position):
        positions = [position]

        while len(positions) > 0:
            current_position = positions.pop()

            for pos in self.__get_neighbour_positions(current_position):

                possible_prototypes = self.__get_possible_prototypes(pos)
                possible_neighbours = self.__get_possible_neighbours(current_position, pos)

                for prototype in possible_prototypes:
                    if prototype not in possible_neighbours:
                        self.__constrain(pos, prototype)
                        if pos not in positions:
                            positions.append(pos)

    def __get_min_entropy_position(self):
        positions = []
        min_entropy = self.__max_entropy

        for x in range(self.x_length):
            for y in range(self.y_length):
                entropy = WFCMap.get_entropy(self.tiles[x][y])
                if (Pos(x, y) not in self.__collapsed) and entropy < min_entropy:
                    min_entropy = entropy
                    positions.clear()
                    positions.append(Pos(x, y))
                elif (Pos(x, y) not in self.__collapsed) and entropy == min_entropy:
                    positions.append(Pos(x, y))

        return random.choice(positions)

    def __show_tile(self, prototype, position):

        tile = prototype.get_tile()

        tile.location.x = self.start_x + position.x * self.tile_size_x
        tile.location.y = self.start_y + position.y * self.tile_size_y
        tile.location.z = 0

        bpy.data.collections[self.__result_collection_name].objects.link(tile)

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

    def __get_neighbour_positions(self, position):
        positions = []

        if position.x - 1 >= 0:
            positions.append(Pos(position.x - 1, position.y))
        if position.y - 1 >= 0:
            positions.append(Pos(position.x, position.y - 1))
        if position.x + 1 < self.x_length:
            positions.append(Pos(position.x + 1, position.y))
        if position.y + 1 < self.y_length:
            positions.append(Pos(position.x, position.y + 1))

        return positions

    def __get_possible_prototypes(self, position):
        return self.tiles[position.x][position.y]

    def __get_possible_neighbours(self, position_a, position_b):
        neighbours = set()

        if len(self.tiles[position_a.x][position_a.y]) == 0:
            raise ConflictException('Contradiction')

        for prototype in self.tiles[position_a.x][position_a.y]:
            for neighbour in prototype.valid_neighbours[get_side(position_a, position_b)]:
                neighbours.add(neighbour)

        return neighbours

    def __constrain(self, position, prototype):
        self.tiles[position.x][position.y] = self.tiles[position.x][position.y] - {prototype}

    def clear(self):
        for object in bpy.data.collections[self.__result_collection_name].objects:
            object.select_set(True)
            bpy.ops.object.delete()
