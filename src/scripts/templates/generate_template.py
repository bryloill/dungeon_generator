import bpy

from tile_prototype import TilePrototype
from wfc_map import WFCMap
from enum import IntEnum
from numpy.random import choice
import utils

TILE_PROTOTYPES = set()
TILE_PROTOTYPES_DICT = dict()


def entropy_func(tile): # setup the entropy func
    pass


def choice_func(prototypes): # setup the choice func
    pass


class Type(IntEnum):  # create own types
    pass

# create raw tiles list
raw = {}

# create tile prototypes
def create_prototypes(map_info):
    pass

# set tile prototypes valid neighbours
def set_valid_neighbours():
    pass

 # run func
def run(map_info):
    create_prototypes(map_info)
    set_valid_neighbours()

    WFCMap.weighted_choice = choice_func
    WFCMap.get_entropy = entropy_func

    generated = False

    while not generated:
        try:
            wfc_map = WFCMap(map_info, TILE_PROTOTYPES)
            while not wfc_map.is_collapsed():
                wfc_map.iterate()
            generated = True
        except utils.ConflictException:
            del wfc_map

    print('Done')
