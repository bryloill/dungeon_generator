import sys

PACKAGES_PATH = 'PATH TO SCRIPTS FOLDER'
sys.path.insert(0, PACKAGES_PATH)

import generate_* as generator # import own implementation

map_info = {
    'X_LENGTH': 5, # map width
    'Y_LENGTH': 5, # map height
    'RESULT_COLLECTION_NAME': 'Result', # name of the collection ib Blender where to put generated objects
    'START_X': 1, # generation start offset | left bottom corner of the map
    'START_Y': 1, # generation start offset | left bottom corner of the map
    'TILE_SIZE_X': 2, # x size of the tile from the tile set in metres
    'TILE_SIZE_Y': 2, # y size of the tile from the tile set in metres
    'PROBABILITIES': {
        generator.Type.WATER: 0.5,      # set the probability of each type
        generator.Type.COAST: 0.05,     #
        generator.Type.LAND: 0.3,       #
        generator.Type.MOUNTAIN: 0.1,   #
        generator.Type.SNOW: 0.05       #
    },
    # 'ROUNDS': 30 add your own configuration variables
}

generator.run(map_info) # call the run func 
