from enum import IntEnum
from collections import namedtuple

Pos = namedtuple('Pos', ['x', 'y'])


class ConflictException(Exception):
    pass


def get_side(position_a, position_b):
    if position_a.x > position_b.x:
        return 'nX'
    elif position_a.x < position_b.x:
        return 'pX'
    if position_a.y > position_b.y:
        return 'nY'
    elif position_a.y < position_b.y:
        return 'pY'


def rotate_sockets(sockets):
    return {'pX': sockets['pY'], 'nX': sockets['nY'], 'pY': sockets['nX'], 'nY': sockets['pX']}


def get_opposite_side(side):
    return side.replace('p', 'n') if side[0] == 'p' else side.replace('n', 'p')


def is_symmetrical(socket):
    return socket[-1] == 's'


def is_asymmetrical(socket):
    return not is_symmetrical(socket) and socket[0].isdigit()


def fits_symmetrical(socket_a, socket_b):
    return socket_a == socket_b


def fits_asymmetrical(socket_a, socket_b):
    s_a = set(socket_a)
    s_b = set(socket_b)

    return (len(s_a) - len(s_b) == 1 and s_a - s_b == set('f')) or (len(s_a) - len(s_b) == -1 and s_b - s_a == set('f'))
