import bpy
import math

class TilePrototype:
    name: str
    obj: object
    rotation: int
    sockets: dict
    valid_neighbours: dict
    weight: float

    def __init__(self, name, obj, rotation, sockets):
        self.name = name
        self.obj = obj
        self.rotation = rotation
        self.sockets = sockets.copy()
        self.valid_neighbours = {
            'pX': None,
            'nX': None,
            'pY': None,
            'nY': None
        }

    def get_tile(self) -> object:
        bpy.ops.object.select_all(action='DESELECT')
        tile = bpy.data.objects.new(f'{self.name} r:{self.rotation}', self.obj.data)
        tile.rotation_euler[2] = math.radians(self.rotation * (-90))
        return tile

    def __repr__(self):
        return f'Prototype {self.name} with rotation {self.rotation}'

    def __eq__(self, other):
        if isinstance(other, TilePrototype):
            return (self.name == other.name) and (self.rotation == other.rotation)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__repr__())
