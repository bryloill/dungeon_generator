import bpy

from tile_prototype import TilePrototype
from wfc_map import WFCMap
from enum import IntEnum
from numpy.random import choice
import utils

TILE_PROTOTYPES = set()
TILE_PROTOTYPES_DICT = dict()


def entropy_func(tile):
    prototypes = list(tile)
    return len(prototypes)


def choice_func(prototypes):
    weights_sum = sum([x.weight for x in prototypes])
    weights = [x.weight / weights_sum for x in prototypes]
    return choice(prototypes, p=weights)


class Type(IntEnum):
    WATER = 1
    COAST = 2
    LAND = 3
    MOUNTAIN = 4
    SNOW = 5


raw = {
    '1_Water': {
        'neighbours': {
            'pX': ['1_Water', '2_Coast'],
            'nX': ['1_Water', '2_Coast'],
            'pY': ['1_Water', '2_Coast'],
            'nY': ['1_Water', '2_Coast']
        },
        'type': Type.WATER
    },
    '2_Coast': {
        'neighbours': {
            'pX': ['1_Water', '2_Coast', '3_Land'],
            'nX': ['1_Water', '2_Coast', '3_Land'],
            'pY': ['1_Water', '2_Coast', '3_Land'],
            'nY': ['1_Water', '2_Coast', '3_Land']
        },
        'type': Type.COAST
    },
    '3_Land': {
        'neighbours': {
            'pX': ['2_Coast', '3_Land', '4_Mountain'],
            'nX': ['2_Coast', '3_Land', '4_Mountain'],
            'pY': ['2_Coast', '3_Land', '4_Mountain'],
            'nY': ['2_Coast', '3_Land', '4_Mountain']
        },
        'type': Type.LAND
    },
    '4_Mountain': {
        'neighbours': {
            'pX': ['3_Land', '4_Mountain', '5_Snow'],
            'nX': ['3_Land', '4_Mountain', '5_Snow'],
            'pY': ['3_Land', '4_Mountain', '5_Snow'],
            'nY': ['3_Land', '4_Mountain', '5_Snow']
        },
        'type': Type.MOUNTAIN
    },
    '5_Snow': {
        'neighbours': {
            'pX': ['4_Mountain', '5_Snow'],
            'nX': ['4_Mountain', '5_Snow'],
            'pY': ['4_Mountain', '5_Snow'],
            'nY': ['4_Mountain', '5_Snow']
        },
        'type': Type.SNOW
    }
}


def create_prototypes(map_info):

    TILE_PROTOTYPES.clear()
    TILE_PROTOTYPES_DICT.clear()

    p_sum = sum([p for type, p in map_info['PROBABILITIES'].items()])
    if abs(p_sum - 1) >= 1e-05:
        raise ValueError("Probability sum is different from 1, actual: " + str(p_sum))

    for land, data in raw.items():
        tile_prototype = TilePrototype(name=land,
                                       obj=bpy.data.objects[land],
                                       rotation=0,
                                       sockets=dict()
                                       )
        tile_prototype.weight = map_info['PROBABILITIES'][data["type"]]
        TILE_PROTOTYPES.add(tile_prototype)
        TILE_PROTOTYPES_DICT[land] = tile_prototype


def set_valid_neighbours():
    for prototype in TILE_PROTOTYPES:
        prototype.valid_neighbours = dict()
        for side, neighbours in raw[prototype.name]['neighbours'].items():
            valid_neighbours = set()
            for neighbour in neighbours:
                valid_neighbours.add(TILE_PROTOTYPES_DICT[neighbour])
            prototype.valid_neighbours[side] = valid_neighbours


def run(map_info):
    create_prototypes(map_info)
    set_valid_neighbours()

    WFCMap.weighted_choice = choice_func
    WFCMap.get_entropy = entropy_func

    generated = False

    while not generated:
        try:
            wfc_map = WFCMap(map_info, TILE_PROTOTYPES)
            while not wfc_map.is_collapsed():
                wfc_map.iterate()
            generated = True
        except utils.ConflictException:
            del wfc_map

    print('Done')
