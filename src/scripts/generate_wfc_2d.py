import bpy

from tile_prototype import TilePrototype
from wfc_map import WFCMap
from enum import IntEnum
from numpy.random import choice
import utils

TILE_PROTOTYPES = set()
TILE_PROTOTYPES_DICT = dict()


class Type(IntEnum):
    ROOM = 1
    CORRIDOR = 2
    OTHER = 3
    EMPTY = 4


def entropy_func(tile):
    prototypes = list(tile)
    return len(prototypes)


def choice_func(prototypes):
    weights_sum = sum([x.weight for x in prototypes])
    weights = [x.weight / weights_sum for x in prototypes]
    return choice(prototypes, p=weights)


raw = {
    '0': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '0s'
        },
        'type': Type.ROOM
    },
    '1': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.ROOM
    },
    '2': {
        'sockets': {
            'pX': '0s',
            'nX': '1s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.ROOM
    },
    '3': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.CORRIDOR
    },
    '4': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '0s'
        },
        'type': Type.CORRIDOR
    },
    '5': {
        'sockets': {
            'pX': '0s',
            'nX': '1s',
            'pY': '3',
            'nY': '3f'
        },
        'type': Type.OTHER
    },
    '6': {
        'sockets': {
            'pX': '3',
            'nX': '3',
            'pY': '3f',
            'nY': '3f'
        },
        'type': Type.OTHER
    },
    '7': {
        'sockets': {
            'pX': '4',
            'nX': '4f',
            'pY': '0s',
            'nY': '5s'
        },
        'type': Type.OTHER
    },
    '8': {
        'sockets': {
            'pX': '4',
            'nX': '4f',
            'pY': '1s',
            'nY': '5s'
        },
        'type': Type.ROOM
    },
    '9': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.ROOM
    },
    '10': {
        'sockets': {
            'pX': '3',
            'nX': '1s',
            'pY': '3f',
            'nY': '1s'
        },
        'type': Type.OTHER
    },
    '11': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.ROOM
    },
    '12': {
        'sockets': {
            'pX': '1s',
            'nX': '0s',
            'pY': '1s',
            'nY': '0s'
        },
        'type': Type.CORRIDOR
    },
    '13': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.ROOM
    },
    '14': {
        'sockets': {
            'pX': '5s',
            'nX': '5s',
            'pY': '5s',
            'nY': '5s'
        },
        'type': Type.OTHER
    },
    '15': {
        'sockets': {
            'pX': '0s',
            'nX': '0s',
            'pY': '1s',
            'nY': '0s'
        },
        'type': Type.CORRIDOR
    },
    '16': {
        'sockets': {
            'pX': '0s',
            'nX': '2f',
            'pY': '0s',
            'nY': '2'
        },
        'type': Type.OTHER
    },
    '17': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.CORRIDOR
    },
    '18': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.ROOM
    },
    '19': {
        'sockets': {
            'pX': '0s',
            'nX': '0s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.CORRIDOR
    },
    '20': {
        'sockets': {
            'pX': '0s',
            'nX': '0s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.CORRIDOR
    },
    '21': {
        'sockets': {
            'pX': '4',
            'nX': '4f',
            'pY': '1s',
            'nY': '5s'
        },
        'type': Type.CORRIDOR
    },
    '22': {
        'sockets': {
            'pX': '4',
            'nX': '5s',
            'pY': '4f',
            'nY': '5s'
        },
        'type': Type.OTHER
    },
    '23': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.CORRIDOR
    },
    '24': {
        'sockets': {
            'pX': '0s',
            'nX': '0s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.EMPTY
    }
}


def create_prototypes(map_info):
    TILE_PROTOTYPES.clear()
    TILE_PROTOTYPES_DICT.clear()

    types_weights = {
        Type.ROOM: 0,
        Type.CORRIDOR: 0,
        Type.OTHER: 0,
        Type.EMPTY: 0
    }

    p_sum = sum([p for type, p in map_info['PROBABILITIES'].items()])
    if abs(p_sum - 1) >= 1e-05:
        raise ValueError("Probability sum is different from 1, actual: " + str(p_sum))

    for type in types_weights.keys():
        type_sum = len([name for name, data in raw.items() if data['type'] == type]) * 4
        if type_sum != 0:
            types_weights[type] = map_info['PROBABILITIES'][type] / type_sum

    for name, data in raw.items():

        rotated_sockets = data['sockets'].copy()

        for rotation in range(0, 4):
            if rotation != 0:
                rotated_sockets = utils.rotate_sockets(rotated_sockets)

            tile_prototype = TilePrototype(name=name,
                                           obj=bpy.data.objects[name],
                                           rotation=rotation,
                                           sockets=rotated_sockets
                                           )

            tile_prototype.type = data['type']
            tile_prototype.weight = types_weights[tile_prototype.type]
            TILE_PROTOTYPES.add(tile_prototype)

            if name not in TILE_PROTOTYPES_DICT.keys():
                TILE_PROTOTYPES_DICT[name] = dict()

            TILE_PROTOTYPES_DICT[name][rotation] = tile_prototype


def set_valid_neighbours():
    for prototype in TILE_PROTOTYPES:
        for other_prototype in TILE_PROTOTYPES:

            for side, socket in prototype.sockets.items():

                if prototype.valid_neighbours[side] is None:
                    prototype.valid_neighbours[side] = set()

                if utils.is_symmetrical(socket) and utils.fits_symmetrical(socket,
                                                                           other_prototype.sockets[
                                                                               utils.get_opposite_side(side)]):
                    prototype.valid_neighbours[side].add(other_prototype)
                    continue

                if utils.is_asymmetrical(socket) and utils.fits_asymmetrical(socket,
                                                                             other_prototype.sockets[
                                                                                 utils.get_opposite_side(side)]):
                    prototype.valid_neighbours[side].add(other_prototype)
                    continue


def run(map_info):
    create_prototypes(map_info)
    set_valid_neighbours()

    WFCMap.weighted_choice = choice_func
    WFCMap.get_entropy = entropy_func

    generated = False

    while not generated:
        try:
            wfc_map = WFCMap(map_info, TILE_PROTOTYPES)
            while not wfc_map.is_collapsed():
                wfc_map.iterate()
            generated = True
        except utils.ConflictException:
            del wfc_map

    print('Done')
