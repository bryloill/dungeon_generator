import bpy

from tile_prototype import TilePrototype
from wfc_map import WFCMap
from numpy.random import choice
from enum import IntEnum
import utils

TILE_PROTOTYPES = set()
TILE_PROTOTYPES_DICT = dict()


class Type(IntEnum):
    FLOOR = 1
    WALL = 2
    INTERSECTION = 3
    DOOR = 4
    OTHER = 5
    EDGE_WALL = 6


def entropy_func(tile):
    prototypes = list(tile)
    return len(prototypes)


def choice_func(prototypes):
    weights_sum = sum([x.weight for x in prototypes])
    weights = [x.weight / weights_sum for x in prototypes]
    return choice(prototypes, p=weights)


raw = {
    'Bottom_floor_level': {
        'sockets': {
            'pX': '2s',
            'nX': '2s',
            'pY': '2s',
            'nY': '2s'
        },
        'type': Type.OTHER
    },
    'Bottom_floor_wall': {
        'sockets': {
            'pX': '4',
            'nX': '4f',
            'pY': '0s',
            'nY': '2s'
        },
        'type': Type.OTHER
    },
    'Bottom_floor_wall_corner': {
        'sockets': {
            'pX': '0s',
            'nX': '4',
            'pY': '0s',
            'nY': '4f'
        },
        'type': Type.OTHER
    },
    'Bottom_floor_wall_corner_round': {
        'sockets': {
            'pX': '4f',
            'nX': '0s',
            'pY': '0s',
            'nY': '4'
        },
        'type': Type.OTHER
    },

    # ------------------------------------------------------- #

    'Main_floor_level': {
        'sockets': {
            'pX': '0s',
            'nX': '0s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.FLOOR
    },

    # ------------------------------------------------------- #

    'Stairs_down': {
        'sockets': {
            'pX': '5',
            'nX': '5f',
            'pY': '0s',
            'nY': '2s'
        },
        'type': Type.OTHER
    },
    'Stairs_down_end': {
        'sockets': {
            'pX': '5',
            'nX': '0s',
            'pY': '0s',
            'nY': '2s'
        },
        'type': Type.OTHER
    },
    'Stairs_down_round_corner_concave': {
        'sockets': {
            'pX': '5',
            'nX': '2s',
            'pY': '5f',
            'nY': '2s'
        },
        'type': Type.OTHER
    },
    'Stairs_down_round_corner_convex': {
        'sockets': {
            'pX': '5',
            'nX': '0s',
            'pY': '0s',
            'nY': '5f'
        },
        'type': Type.OTHER
    },
    'Stairs_top': {
        'sockets': {
            'pX': '6',
            'nX': '6f',
            'pY': '1s',
            'nY': '0s'
        },
        'type': Type.OTHER
    },
    'Stairs_top_round_corner_concave': {
        'sockets': {
            'pX': '6',
            'nX': '0s',
            'pY': '6f',
            'nY': '0s'
        },
        'type': Type.OTHER
    },
    'Stairs_top_round_corner_convex': {
        'sockets': {
            'pX': '6',
            'nX': '1s',
            'pY': '1s',
            'nY': '6f'
        },
        'type': Type.OTHER
    },

    # ------------------------------------------------------- #

    'Top_floor_level': {
        'sockets': {
            'pX': '1s',
            'nX': '1s',
            'pY': '1s',
            'nY': '1s'
        },
        'type': Type.OTHER
    },
    'Wall_arch': {
        'sockets': {
            'pX': '3s',
            'nX': '3s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.DOOR
    },
    'Wall_ending': {
        'sockets': {
            'pX': '3s',
            'nX': '0s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.FLOOR
    },
    'Wall_straight': {
        'sockets': {
            'pX': '3s',
            'nX': '3s',
            'pY': '0s',
            'nY': '0s'
        },
        'type': Type.WALL
    },
    'Wall_straight_corner': {
        'sockets': {
            'pX': '3s',
            'nX': '0s',
            'pY': '3s',
            'nY': '0s'
        },
        'type': Type.WALL
    },
    'Wall_straight_T_intersection': {
        'sockets': {
            'pX': '3s',
            'nX': '3s',
            'pY': '3s',
            'nY': '0s'
        },
        'type': Type.INTERSECTION
    },

    # ------------------------------------------------------- #
    'Wall_straight_edge': {
        'sockets': {
            'pX': '7',
            'nX': '7f',
            'pY': '0s',
            'nY': '-1s'
        },
        'type': Type.EDGE_WALL
    },

    'Wall_corner_edge': {
        'sockets': {
            'pX': '7f',
            'nX': '-1s',
            'pY': '-1s',
            'nY': '7'
        },
        'type': Type.EDGE_WALL
    },

    'Wall_outer_corner_edge': {
        'sockets': {
            'pX': '7f',
            'nX': '0s',
            'pY': '7',
            'nY': '0s'
        },
        'type': Type.EDGE_WALL
    },

    'Wall_straight_T_intersection_edge': {
        'sockets': {
            'pX': '7',
            'nX': '7f',
            'pY': '3s',
            'nY': '-1s'
        },
        'type': Type.EDGE_WALL
    },

    # ------------------------------------------------------- #
    'Empty': {
        'sockets': {
            'pX': '-1s',
            'nX': '-1s',
            'pY': '-1s',
            'nY': '-1s'
        },
        'type': Type.OTHER
    }
}


def create_prototypes(map_info):

    TILE_PROTOTYPES.clear()
    TILE_PROTOTYPES_DICT.clear()

    types_weights = {
        Type.WALL: 0,
        Type.DOOR: 0,
        Type.FLOOR: 0,
        Type.INTERSECTION: 0,
        Type.OTHER: 0,
        Type.EDGE_WALL: 0
    }

    p_sum = sum([p for type, p in map_info['PROBABILITIES'].items()])
    if abs(p_sum - 1) >= 1e-05:
        raise ValueError("Probability sum is different from 1, actual: " + str(p_sum))

    for type in types_weights.keys():
        type_sum = len([land for land, data in raw.items() if data['type'] == type]) * 4
        if type_sum != 0:
            types_weights[type] = map_info['PROBABILITIES'][type] / type_sum

    for land, data in raw.items():

        rotated_sockets = data['sockets'].copy()

        for rotation in range(0, 4):
            if rotation != 0:
                rotated_sockets = utils.rotate_sockets(rotated_sockets)

            tile_prototype = TilePrototype(name=land,
                                           obj=bpy.data.objects[land],
                                           rotation=rotation,
                                           sockets=rotated_sockets
                                           )

            tile_prototype.type = data['type']
            tile_prototype.weight = types_weights[tile_prototype.type]
            TILE_PROTOTYPES.add(tile_prototype)

            if land not in TILE_PROTOTYPES_DICT.keys():
                TILE_PROTOTYPES_DICT[land] = dict()

            TILE_PROTOTYPES_DICT[land][rotation] = tile_prototype


def set_valid_neighbours():
    for prototype in TILE_PROTOTYPES:
        for other_prototype in TILE_PROTOTYPES:

            for side, socket in prototype.sockets.items():

                if prototype.valid_neighbours[side] is None:
                    prototype.valid_neighbours[side] = set()

                if utils.is_symmetrical(socket) and utils.fits_symmetrical(socket,
                                                                           other_prototype.sockets[
                                                                               utils.get_opposite_side(side)]):
                    prototype.valid_neighbours[side].add(other_prototype)
                    continue

                if utils.is_asymmetrical(socket) and utils.fits_asymmetrical(socket,
                                                                             other_prototype.sockets[
                                                                                 utils.get_opposite_side(side)]):
                    prototype.valid_neighbours[side].add(other_prototype)
                    continue


def prepare_edges(wfc_map, x_length, y_length, p):
    if not 0 < p < 1:
        raise Exception

    for x, y in [(x, 0) for x in range(1, x_length - 1)]:
        tiles = [x for x in TILE_PROTOTYPES if (x.name == 'Wall_straight_edge' or
                                                x.name == 'Wall_straight_T_intersection_edge') and (x.rotation == 0)]
        tiles.sort(key=lambda x: x.name)
        wfc_map.tiles[x][y] = {choice(tiles, p=[p, 1 - p])}

    for x, y in [(x, y_length - 1) for x in range(1, x_length - 1)]:
        tiles = [x for x in TILE_PROTOTYPES if (x.name == 'Wall_straight_edge' or
                                                x.name == 'Wall_straight_T_intersection_edge') and (x.rotation == 2)]
        tiles.sort(key=lambda x: x.name)
        wfc_map.tiles[x][y] = {choice(tiles, p=[p, 1 - p])}

    for x, y in [(0, y) for y in range(1, y_length - 1)]:
        tiles = [x for x in TILE_PROTOTYPES if (x.name == 'Wall_straight_edge' or
                                                x.name == 'Wall_straight_T_intersection_edge') and (x.rotation == 1)]
        tiles.sort(key=lambda x: x.name)
        wfc_map.tiles[x][y] = {choice(tiles, p=[p, 1 - p])}

    for x, y in [(x_length - 1, y) for y in range(1, y_length - 1)]:
        tiles = [x for x in TILE_PROTOTYPES if (x.name == 'Wall_straight_edge' or
                                                x.name == 'Wall_straight_T_intersection_edge') and (x.rotation == 3)]
        tiles.sort(key=lambda x: x.name)
        wfc_map.tiles[x][y] = {choice(tiles, p=[p, 1 - p])}


def run(map_info):
    create_prototypes(map_info)
    set_valid_neighbours()

    WFCMap.weighted_choice = choice_func
    WFCMap.get_entropy = entropy_func

    generated = False

    while not generated:
        try:
            wfc_map = WFCMap(map_info, TILE_PROTOTYPES)
            prepare_edges(wfc_map, map_info['X_LENGTH'], map_info['Y_LENGTH'], map_info['SEGMENTATION'])
            while not wfc_map.is_collapsed():
                wfc_map.iterate()
            generated = True
        except utils.ConflictException:
            wfc_map.clear()

    print("Done")
