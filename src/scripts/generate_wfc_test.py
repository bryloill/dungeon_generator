import bpy

from tile_prototype import TilePrototype
from wfc_map import WFCMap
from enum import Enum
from numpy.random import choice
import utils

TILE_PROTOTYPES = set()
TILE_PROTOTYPES_DICT = dict()


class Type(str, Enum):
    WATER = 'WATER'
    COAST = 'COAST'
    LAND = 'LAND'
    MOUNTAIN = 'MOUNTAIN'
    SNOW = 'SNOW'


RULES_TEST = list()

TYPES_COUNT = {
    Type.WATER: 0,
    Type.COAST: 0,
    Type.LAND: 0,
    Type.MOUNTAIN: 0,
    Type.SNOW: 0
}


def entropy_func(tile):
    prototypes = list(tile)
    return len(prototypes)


def choice_func(prototypes):
    weights_sum = sum([x.weight for x in prototypes])
    weights = [x.weight / weights_sum for x in prototypes]
    return choice(prototypes, p=weights)


def get_neighbour_positions(position, x_length, y_length):
    positions = []

    if position.x - 1 >= 0:
        positions.append(utils.Pos(position.x - 1, position.y))
    if position.y - 1 >= 0:
        positions.append(utils.Pos(position.x, position.y - 1))
    if position.x + 1 < x_length:
        positions.append(utils.Pos(position.x + 1, position.y))
    if position.y + 1 < y_length:
        positions.append(utils.Pos(position.x, position.y + 1))

    return positions


raw = {
    '1_Water': {
        'neighbours': {
            'pX': ['1_Water', '2_Coast'],
            'nX': ['1_Water', '2_Coast'],
            'pY': ['1_Water', '2_Coast'],
            'nY': ['1_Water', '2_Coast']
        },
        'type': Type.WATER
    },
    '2_Coast': {
        'neighbours': {
            'pX': ['1_Water', '2_Coast', '3_Land'],
            'nX': ['1_Water', '2_Coast', '3_Land'],
            'pY': ['1_Water', '2_Coast', '3_Land'],
            'nY': ['1_Water', '2_Coast', '3_Land']
        },
        'type': Type.COAST
    },
    '3_Land': {
        'neighbours': {
            'pX': ['2_Coast', '3_Land', '4_Mountain'],
            'nX': ['2_Coast', '3_Land', '4_Mountain'],
            'pY': ['2_Coast', '3_Land', '4_Mountain'],
            'nY': ['2_Coast', '3_Land', '4_Mountain']
        },
        'type': Type.LAND
    },
    '4_Mountain': {
        'neighbours': {
            'pX': ['3_Land', '4_Mountain', '5_Snow'],
            'nX': ['3_Land', '4_Mountain', '5_Snow'],
            'pY': ['3_Land', '4_Mountain', '5_Snow'],
            'nY': ['3_Land', '4_Mountain', '5_Snow']
        },
        'type': Type.MOUNTAIN
    },
    '5_Snow': {
        'neighbours': {
            'pX': ['4_Mountain', '5_Snow'],
            'nX': ['4_Mountain', '5_Snow'],
            'pY': ['4_Mountain', '5_Snow'],
            'nY': ['4_Mountain', '5_Snow']
        },
        'type': Type.SNOW
    }
}


def create_prototypes(map_info):
    TILE_PROTOTYPES.clear()
    TILE_PROTOTYPES_DICT.clear()

    p_sum = sum([p for type, p in map_info['PROBABILITIES'].items()])
    if abs(p_sum - 1) >= 1e-05:
        raise ValueError("Probability sum is different from 1, actual: " + str(p_sum))

    for land, data in raw.items():
        tile_prototype = TilePrototype(name=land,
                                       obj=bpy.data.objects[land],
                                       rotation=0,
                                       sockets=dict(),
                                       )
        tile_prototype.weight = map_info['PROBABILITIES'][data["type"]]
        TILE_PROTOTYPES.add(tile_prototype)
        TILE_PROTOTYPES_DICT[land] = tile_prototype


def set_valid_neighbours():
    for prototype in TILE_PROTOTYPES:
        prototype.valid_neighbours = dict()
        for side, neighbours in raw[prototype.name]['neighbours'].items():
            valid_neighbours = set()
            for neighbour in neighbours:
                valid_neighbours.add(TILE_PROTOTYPES_DICT[neighbour])
            prototype.valid_neighbours[side] = valid_neighbours


def test_rules(wfc_map):
    global RULES_TEST

    for x in range(wfc_map.x_length):
        for y in range(wfc_map.y_length):
            tile = list(wfc_map.tiles[x][y])[0]
            neighbour_pos = get_neighbour_positions(utils.Pos(x, y), wfc_map.x_length, wfc_map.y_length)

            for pos in neighbour_pos:
                if tile not in tile.valid_neighbours[utils.get_side(pos, utils.Pos(x, y))]:
                    RULES_TEST.append(False)
                    return

    RULES_TEST.append(True)


def record_prototypes_count(wfc_map):
    global TYPES_COUNT
    for x in range(wfc_map.x_length):
        for y in range(wfc_map.y_length):
            tile = list(wfc_map.tiles[x][y])[0]
            TYPES_COUNT[tile.obj.name.split('_')[1].upper()] += 1


def show_results(wfc_map, map_info, rounds):
    print("------------------- NEIGHBOURS VALID: -------------------")
    for i in range(0, len(RULES_TEST)):
        print(f'{i}: {"Success" if RULES_TEST[i] else "Failure"}')

    print("------------------- PROBABILITY DISTRIBUTION: -------------------")
    value_sum = sum([value for key, value in TYPES_COUNT.items()])
    for key, value in TYPES_COUNT.items():
        print(f'{key}: {round(value / value_sum, 3)}, expected: {map_info["PROBABILITIES"][key]}')


def inner_run(wfc_map, map_info):
    generated = False

    while not generated:
        try:
            wfc_map = WFCMap(map_info, TILE_PROTOTYPES)
            while not wfc_map.is_collapsed():
                wfc_map.iterate()
            generated = True
            test_rules(wfc_map)
            record_prototypes_count(wfc_map)
            wfc_map.clear()
            del wfc_map
        except utils.ConflictException:
            del wfc_map


def run(map_info):

    RULES_TEST.clear()
    for key, value in TYPES_COUNT.items():
        TYPES_COUNT[key] = 0

    create_prototypes(map_info)
    set_valid_neighbours()

    WFCMap.weighted_choice = choice_func
    WFCMap.get_entropy = entropy_func

    wfc_map = WFCMap(map_info, TILE_PROTOTYPES)

    rounds = map_info['ROUNDS']
    for i in range(0, rounds):
        inner_run(wfc_map, map_info)

    show_results(wfc_map, map_info, rounds)

    print('Done')
